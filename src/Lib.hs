{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib
    (
      apiWebServer
    , Config(configDbConnString)
    , runWithConfig
    , apiWebServerIO
    ) where

import Polysemy
import Polysemy.Reader
import Polysemy.WebServer
import Polysemy.Error
import Polysemy.ConstraintAbsorber.MonadCatch
import qualified Network.Wai as Wai
import Logging
import Control.Monad
import qualified Network.HTTP.Types as HTTP
import qualified Data.Aeson as JSON
import Config (Config(..), runWithConfig)

jsonResponse :: (JSON.ToJSON a, WebServer `Member` r) =>
  PendingWebRequest -> HTTP.Status -> a -> Sem r Wai.ResponseReceived
jsonResponse pendingReq status d =
  respondWebRequest pendingReq (
    Wai.responseLBS status [(HTTP.hContentType, "application/json")] (JSON.encode d)
  )

privateApiWebHandler :: WebServer `Member` r =>
  Wai.Request -> PendingWebRequest -> Sem r Wai.ResponseReceived
privateApiWebHandler req pendingReq =
  case (Wai.pathInfo req, Wai.requestMethod req) of
    (["v1", "pending-requests"], "GET") ->
      jsonResponse @[()] pendingReq HTTP.ok200 []
    _ -> respondWebRequest pendingReq
           (Wai.responseLBS HTTP.notFound404 [] "Not found")

publicApiWebHandler :: WebServer `Member` r =>
  Wai.Request -> PendingWebRequest -> Sem r Wai.ResponseReceived
publicApiWebHandler req pendingReq =
  case (Wai.pathInfo req, Wai.requestMethod req) of
    (["v1", "request"], "POST") ->
      jsonResponse @[()] pendingReq HTTP.ok200 []
    _ -> respondWebRequest pendingReq
           (Wai.responseLBS HTTP.notFound404 [] "Not found")

apiWebServer :: (WebServer `Member` r, Reader Config `Member` r,
                 Error SomeException `Member` r, LogOutput `Member` r) => Sem r ()
apiWebServer = do
  publicPort <- asks configPublicPort
  privatePort <- asks configPrivatePort
  startWebServer privatePort privateApiWebHandler
  startWebServer publicPort publicApiWebHandler

apiWebServerIO :: IO ()
apiWebServerIO = void $
  runFinal . embedToFinal . runLogIOFinal . errorToIOFinalUnwrapped .
  logExceptions . runWithConfig . runWebServerFinal $ apiWebServer
