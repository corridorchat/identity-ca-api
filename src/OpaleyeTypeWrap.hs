-- It potentially should be a common library.
-- This file is licensed under different terms to the rest of
-- this API:
{-
Copyright 2022 Andrew Miller

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
{-# LANGUAGE TemplateHaskell #-}
module OpaleyeTypeWrap where
import Language.Haskell.TH.Syntax
import Data.Profunctor.Product.Default
import Opaleye

opaleyeTypeWrap :: String -> Q Type -> Q Type -> Q [Dec]
opaleyeTypeWrap nameStr qInnerTypeHaskell qInnerTypeSql = do
  innerTypeHaskell <- qInnerTypeHaskell
  innerTypeSql <- qInnerTypeSql
  let
    namePrime = mkName (nameStr ++ "'")
    name = mkName nameStr
    nameField = mkName (nameStr ++ "Field")
    nameFieldNullable = mkName (nameStr ++ "FieldNullable")
    unName = mkName ("un" ++ nameStr)
    defN = mkName "def"
  a <- newName "a"
  fieldCon <- [t|Field|] 
  fieldNullableCon <- [t|FieldNullable|] 
  defaultToFields <- [t|Default ToFields $(pure $ ConT name) $(pure $ ConT nameField)|]
  defineDef <- [d|
     $(pure $ VarP defN) =
       dimap $(pure $ VarE unName) $(pure $ ConE name) $(pure $ VarE defN) |]
  defineDefAp <- [d|
     $(pure $ VarP defN) =
       dimap $(pure $ VarE unName) ($(pure $ ConE name) <$>) $(pure $ VarE defN) |]
  defaultFromFields <- [t|Default FromFields $(pure $ ConT nameField) $(pure $ ConT name)|]
  defaultFromFieldsNullable <- [t|Default FromFields $(pure $ ConT nameFieldNullable) (Maybe $(pure $ ConT name))|]
  return $ [
    -- newtype Name' a = Name { unName :: a }
    NewtypeD [] namePrime [PlainTV a ()] Nothing
      (RecC name [(unName, Bang NoSourceUnpackedness NoSourceStrictness, VarT a)]) [],
    -- type Name = Name' innerTypeHaskell
    TySynD name [] (AppT (ConT namePrime) innerTypeHaskell),
    -- type NameField = Name' (Field innerTypeSql)
    TySynD nameField [] (AppT (ConT namePrime) (AppT fieldCon innerTypeSql)), 
    -- type NameFieldNullable = Name' (Field innerTypeSql)
    TySynD nameFieldNullable [] (
        AppT (ConT namePrime) (AppT fieldNullableCon innerTypeSql)),
   -- instance Default ToFields Name NameField where
    --   def = dimap unName Name def
    InstanceD Nothing [] defaultToFields defineDef,
    -- instance Default FromFields NameField NameId where
    --   def = dimap unName Name def
    InstanceD Nothing [] defaultFromFields defineDef,
    -- instance Default FromFields NameFieldNullable (Maybe NameId) where
    --   def = dimap unName (Name <$>) def
    InstanceD Nothing [] defaultFromFieldsNullable defineDefAp
    ]

{- | Takes a name, and a list of Haskell and DB name pairs
     and generates code to use them as an enum

     opaleyeSumTypeInString "Name" [("Foo", "FOO"), ("Bar", "BAR")]
     expands to:

data NameValues = NameCert | NameVersion deriving (Eq, Ord)
data Name' a = Name { unName :: a }
type NameField = Name' (Field SqlText)
type NameFieldNullable = Name' (FieldNullable SqlText)
type Name = Name' (Either String NameValues)
instance Default ToFields Name NameField where
  def = dimap (\v -> Name $ case (unName v) of
                      Left t -> t
                      Right NameFoo -> "FOO" :: String
                      Right NameBar -> "BAR" :: String
              ) Name def
instance Default FromFields NameField Name where
  def = dimap unName (\v -> Name $ case v of
                                 "FOO" -> Right NameFoo
                                 "BAR" -> Right NameBar
                                 t -> Left t
                             ) Name def
instance Default FromFields NameFieldNullable (Maybe Name) where
  def = dimap unName ((\v -> Name $ case v of
                                 "FOO" -> Right NameFoo
                                 "BAR" -> Right NameBar
                                 t -> Left t
                             ) <$>) def
 -}
opaleyeSumTypeInString :: String -> [(String, String)] -> Q [Dec]
opaleyeSumTypeInString nameS haskDBPairs = do
  let name = mkName nameS
      nameValues = mkName (nameS ++ "Values")
      namePrime = mkName (nameS ++ "'")
      nameField = mkName (nameS ++ "Field")
      nameFieldNullable = mkName (nameS ++ "FieldNullable")
      unName = mkName ("un" ++ nameS)
      defN = mkName "def"
  a <- newName "a"
  v <- newName "v"
  t <- newName "t"
  nameFieldDef <- [t| $(pure . ConT $ namePrime) (Field SqlText) |]
  nameFieldNullableDef <-
    [t| $(pure . ConT $ namePrime) (FieldNullable SqlText) |]
  nameDef <- [t| $(pure . ConT $ namePrime)
                 (Either String $(pure . ConT $ nameValues)) |]
  defaultToFields <- [t|Default ToFields $(pure $ ConT name) $(pure $ ConT nameField)|]
  let makeToFieldExpr :: (String, String) -> Q Match
      makeToFieldExpr (hask, sql) = do
        pat <- [p| Right $(pure $ ConP (mkName $ nameS ++ hask) []) |]
        expr <- [e| $(pure . LitE . StringL $ sql) :: String |]
        return $ Match pat (NormalB expr) []
  toFieldsDef <- [d|
                   $(pure . VarP $ defN) =
                     dimap (\ $(pure . VarP $ v) ->
                              $(CaseE (AppE (VarE unName) (VarE v)) <$>
                                 (do
                                    defPat <- [p| Left $(pure . VarP $ t)|]
                                    rightMatches <- mapM makeToFieldExpr haskDBPairs
                                    return $ (Match defPat (NormalB (VarE t)) []):rightMatches
                                 ))
                           ) $(pure . ConE $ name) def |]
  defaultFromFields <- [t|Default FromFields $(pure $ ConT nameField) $(pure $ ConT name) |]
  let makeFromFieldExpr :: (String, String) -> Q Match
      makeFromFieldExpr (hask, sql) = do
        expr <- [e| Right $(pure . ConE . mkName $ nameS ++ hask) |]
        return $ Match (LitP . StringL $ sql) (NormalB expr) []
  fromFieldsDef <- [d|
                   $(pure . VarP $ defN) =
                     dimap $(pure . VarE $ unName)
                           (\ $(pure . VarP $ v) -> $(pure . ConE $ name)
                              $(CaseE (VarE v) <$>
                                 (do
                                    defExpr <- [e| Left $(pure . VarE $ t) |]
                                    rightMatches <- mapM makeFromFieldExpr haskDBPairs
                                    return $ (Match (VarP t) (NormalB defExpr) []):rightMatches
                                 ))
                           ) def |]
  fromFieldsDefAp <- [d|
                     $(pure . VarP $ defN) =
                       dimap $(pure . VarE $ unName)
                             ((\ $(pure . VarP $ v) -> $(pure . ConE $ name)
                                 $(CaseE (VarE v) <$>
                                    (do
                                       defExpr <- [e| Left $(pure . VarE $ t) |]
                                       rightMatches <- mapM makeFromFieldExpr haskDBPairs
                                       return $ (Match (VarP t) (NormalB defExpr) []):rightMatches
                                    ))
                              ) <$>) def |]
  defaultFromFieldsNullable <- [t|Default FromFields $(pure $ ConT nameFieldNullable)
                                          (Maybe $(pure $ ConT name)) |]
  return [
    -- data NameValues = NameCert | NameVersion deriving (Eq, Ord)
    DataD [] nameValues [] Nothing (
        map (\(haskName, _) -> NormalC (mkName $ nameS ++ haskName) []) haskDBPairs)
        [DerivClause Nothing [ConT (mkName "Eq"), ConT (mkName "Ord")]],
    -- data Name' a = Name { unName :: a }
    DataD [] namePrime [PlainTV a ()] Nothing [RecC name [(unName, Bang NoSourceUnpackedness NoSourceStrictness, VarT a)]]
          [],
    -- type NameField = Name' (Field SqlText)
    TySynD nameField [] nameFieldDef,
    -- type NameFieldNullable = Name' (FieldNullable SqlText)
    TySynD nameFieldNullable [] nameFieldNullableDef,
    -- type Name = Name' (Either String NameValues)
    TySynD name [] nameDef,
    -- instance Default ToFields Name NameField where
    --   def = dimap (\v -> case (unName v) of
    --                       Left t -> t
    --                       Right NameFoo -> "FOO" :: String
    --                       Right NameBar -> "BAR" :: String
    --               ) Name def
    InstanceD Nothing [] defaultToFields toFieldsDef,
    -- instance Default FromFields NameField Name where
    --   def = dimap unName (\v -> Name $ case v of
    --                                  "FOO" -> Right NameFoo
    --                                  "BAR" -> Right NameBar
    --                                  t -> Left t
    --                              ) def
    InstanceD Nothing [] defaultFromFields fromFieldsDef,
    -- instance Default FromFields NameFieldNullable (Maybe Name) where
    --   def = dimap unName ((\v -> Name $ case v of
    --                                  "FOO" -> Right NameFoo
    --                                  "BAR" -> Right NameBar
    --                                  t -> Left t
    --                              ) <$>) def
    InstanceD Nothing [] defaultFromFieldsNullable fromFieldsDefAp
    
         ]
 
