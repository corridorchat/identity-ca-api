-- This file provides facilities for logging.
-- It potentially should be a common library.
-- This file is licensed under different terms to the rest of
-- this API:
{-
Copyright 2022 Andrew Miller

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
module Logging where
import Polysemy
import Polysemy.Output
import Polysemy.Error
import Polysemy.Final
import Control.Monad.Logger
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Encoding.Error as TE
import qualified Data.ByteString.Lazy as LBS
import Control.Exception (SomeException)
import Data.Functor
import qualified Data.Aeson as Aeson
import qualified Control.Exception as X

data LogEntry = LogEntry {
    logEntryLoc :: Loc
  , logEntryLogSource :: LogSource
  , logEntryLogLevel :: LogLevel
  , logEntryMsg :: LogStr
                         }

type LogOutput = Output LogEntry
instance LogOutput `Member` r => MonadLogger (Sem r) where
  monadLoggerLog loc src lvl msg =
    output $ LogEntry loc src lvl (toLogStr msg)

-- Modified from polysemy version to not wrap exceptions to allow for interaction
-- with exceptions thrown from IO.
errorToIOFinalUnwrapped
    :: Member (Final IO) r
    => Sem (Error SomeException ': r) a
    -> Sem r (Either SomeException a)
errorToIOFinalUnwrapped sem = withStrategicToFinal @IO $ do
  m' <- runS (runErrorAsExcFinal sem)
  s  <- getInitialStateS
  pure $
    either
      ((<$ s) . Left)
      (fmap Right)
    <$> X.try m'
{-# INLINE errorToIOFinalUnwrapped #-}

runErrorAsExcFinal
    :: forall r a.
       Member (Final IO) r
    => Sem (Error SomeException ': r) a
    -> Sem r a
runErrorAsExcFinal = interpretFinal $ \case
  Throw e   -> pure . X.throwIO $ e
  Catch m h -> do
    m' <- runS m
    h' <- bindS h
    s  <- getInitialStateS
    pure $ X.catch m' $ \(se :: SomeException) ->
      h' (se <$ s)
{-# INLINE runErrorAsExcFinal #-}

runLogIOFinal :: Final IO `Member` r =>
  Sem (LogOutput ': r) a -> Sem r a
runLogIOFinal = interpret $ \(Output (LogEntry loc src lvl msg)) ->
  embedFinal @IO . runStderrLoggingT $
    monadLoggerLog loc src lvl msg
                               
logExceptions :: (LogOutput `Member` r, Error SomeException `Member` r) =>
  Sem r a -> Sem r (Either SomeException a)
logExceptions f = catch (Right <$> f) (\e ->
  $(logError) (tshow e) $> Left e)

tshow :: Show a => a -> T.Text
tshow = T.pack . show

jshow :: Aeson.ToJSON a => a -> T.Text
jshow = TE.decodeUtf8With TE.lenientDecode . LBS.toStrict . Aeson.encode
