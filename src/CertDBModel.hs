{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module CertDBModel where
import Opaleye
import Data.Profunctor
import Data.Profunctor.Product.Default
import Data.Profunctor.Product.TH (makeAdaptorAndInstanceInferrable)
import Data.UUID
import qualified Data.Text as T
import Data.Time.Clock
import Data.Time.Calendar
import OpaleyeTypeWrap
import qualified Data.ByteString as BS
import Data.Int (Int64)

$(opaleyeTypeWrap "PersonId" [t|UUID|] [t|SqlUuid|])
$(makeAdaptorAndInstanceInferrable "pPersonId" ''PersonId')

data Person' personId optText text date ts optTs = Person {
    personId :: personId
  , personCurrentLegalName :: optText
  , personOriginalLegalName :: text
  , personDateOfBirth :: date
  , personCountryOfBirth :: text
  , personPlaceOfBirth :: text
  , personContactEmail :: optText
  , personCreatedAt :: ts
  , personVerifiedAt :: optTs
  , personRevokedAt :: optTs
                                                         }
$(makeAdaptorAndInstanceInferrable "pPerson" ''Person')

type Person = Person' PersonId (Maybe T.Text) T.Text Day UTCTime (Maybe UTCTime)
type PersonField = Person' PersonIdField (FieldNullable SqlText) (Field SqlText) (Field SqlDate) (Field SqlTimestamptz) (FieldNullable SqlTimestamptz)

personTable :: Table PersonField PersonField
personTable = table "person" (
  pPerson Person {
      personId = pPersonId (PersonId (tableField "id"))
    , personCurrentLegalName = tableField "current_legal_name"
    , personOriginalLegalName = tableField "original_legal_name"
    , personDateOfBirth = tableField "date_of_birth"
    , personCountryOfBirth = tableField "country_of_birth"
    , personPlaceOfBirth = tableField "place_of_birth"
    , personContactEmail = tableField "contact_email"
    , personCreatedAt = tableField "created_at"
    , personVerifiedAt = tableField "verified_at"
    , personRevokedAt = tableField "revoked_at"
  })

$(opaleyeSumTypeInString "GapfreeSeqId" [
     ("Cert", "CERTIFICATE"), ("Version", "VERSION")])
$(makeAdaptorAndInstanceInferrable "pGapfreeSeqId" ''GapfreeSeqId')

data GapfreeSequence' seqId nextVal = GapfreeSequence {
    gapfreeSequenceId :: seqId
  , gapfreeSequenceNextValue :: nextVal
                                        }
$(makeAdaptorAndInstanceInferrable "pGapfreeSequence" ''GapfreeSequence')

type GapfreeSequence = GapfreeSequence' GapfreeSeqId Int64
type GapfreeSequenceField = GapfreeSequence' GapfreeSeqIdField (Field SqlInt8)

gapfreeSequenceTable :: Table GapfreeSequenceField GapfreeSequenceField
gapfreeSequenceTable = table "gapfree_sequences" (
  pGapfreeSequence GapfreeSequence {
        gapfreeSequenceId = pGapfreeSeqId (GapfreeSeqId $ tableField "id")
      , gapfreeSequenceNextValue = tableField "next_value"
                                   }
                                                 )

$(opaleyeTypeWrap "CertificateRequestId" [t|UUID|] [t|SqlUuid|])
$(makeAdaptorAndInstanceInferrable "pCertificateRequestId" ''CertificateRequestId')

$(opaleyeSumTypeInString "CertificateRequestResult" [
     ("Approved", "APPROVED"), ("Declined", "DECLINED")
     ])
$(makeAdaptorAndInstanceInferrable "pCertificateRequestResult" ''CertificateRequestResult')

data CertificateRequest' reqId bytes personId ts optTs result = CertificateRequest {
    certReqId :: reqId
  , certReqPrivateEvidence :: bytes
  , certReqClaimedPerson :: personId
  , certReqCreatedAt :: ts
  , certReqProcessedAt :: optTs
  , certReqProcessResult :: result
  , certReqPublicKey :: bytes
  }
$(makeAdaptorAndInstanceInferrable "pCertificateRequest" ''CertificateRequest')
type CertificateRequest = CertificateRequest' CertificateRequestId (Maybe BS.ByteString) PersonId UTCTime (Maybe UTCTime) CertificateRequestResult
type CertificateRequestField = CertificateRequest' CertificateRequestIdField (FieldNullable SqlBytea) PersonIdField (Field SqlTimestamptz) (FieldNullable SqlTimestamptz) CertificateRequestResultField

certificateRequestTable :: Table CertificateRequestField CertificateRequestField
certificateRequestTable = table "certificate_request" (
  pCertificateRequest CertificateRequest {
        certReqId = pCertificateRequestId (
            CertificateRequestId $ tableField "id")
      , certReqPrivateEvidence = tableField "private_evidence"
      , certReqClaimedPerson = pPersonId (PersonId $ tableField "claimed_person")
      , certReqCreatedAt = tableField "created_at"
      , certReqProcessedAt = tableField "processed_at"
      , certReqProcessResult = pCertificateRequestResult (CertificateRequestResult $ tableField "process_result")
      , certReqPublicKey = tableField "public_key"
                                         }
                                                      )

$(opaleyeTypeWrap "CertificateId" [t|Int64|] [t|SqlInt8|])
$(makeAdaptorAndInstanceInferrable "pCertificateId" ''CertificateId')

$(opaleyeTypeWrap "Version" [t|Int64|] [t|SqlInt8|])
$(makeAdaptorAndInstanceInferrable "pVersion" ''Version')

data Certificate' certId reqId bs ts optTs ver = Certificate {
    certId :: certId
  , certCreateRequest :: reqId
  , certCertificate :: bs
  , certValidFrom :: ts
  , certValidTo :: optTs
  , certChangedVersion :: ver
  }
$(makeAdaptorAndInstanceInferrable "pCertificate" ''Certificate')
type Certificate = Certificate' CertificateId CertificateRequestId BS.ByteString UTCTime (Maybe UTCTime) Version
type CertificateField = Certificate' CertificateIdField CertificateRequestIdField (Field SqlBytea) (Field SqlTimestamptz) (FieldNullable SqlTimestamptz) VersionField

certificateTable :: Table CertificateField CertificateField
certificateTable = table "certificate" (
  pCertificate Certificate {
      certId = pCertificateId (CertificateId $ tableField "id")
    , certCreateRequest = pCertificateRequestId (CertificateRequestId $ tableField "create_request")
    , certCertificate = tableField "certificate"
    , certValidFrom = tableField "valid_from"
    , certValidTo = tableField "valid_to"
    , certChangedVersion = pVersion (Version $ tableField "changed_version")
                           }
                                       )
