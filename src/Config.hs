module Config (Config(..), runWithConfig) where
import qualified Network.Wai.Handler.Warp as Warp
import Polysemy
import Polysemy.Reader
import Polysemy.ConstraintAbsorber.MonadCatch
import Polysemy.Error
import System.Environment
import Text.Read

data Config = Config {
    configPublicPort :: Warp.Port
  , configPrivatePort :: Warp.Port
  , configDbConnString :: String
  }
newtype ConfigError = ConfigError String deriving (Eq, Ord, Show)
instance Exception ConfigError

readEnvString :: (Embed IO `Member` r, Error SomeException `Member` r) => String -> Sem r String
readEnvString var =
  maybe (throw . toException . ConfigError $ "Missing environment variable: " <> var) return =<<
    embed (lookupEnv var)

readEnv :: (Embed IO `Member` r, Error SomeException `Member` r, Read a) => String -> Sem r a
readEnv var = do
  s <- readEnvString var
  maybe (throw . toException . ConfigError $ "Malformed environment variable: " <> var) return $
    readMaybe s

loadConfig :: (Embed IO `Member` r, Error SomeException `Member` r) => Sem r Config
loadConfig =
  Config <$> readEnv "IDENTITYCA_PUBLIC_PORT" <*> readEnv "IDENTITYCA_PRIVATE_PORT"
         <*> readEnvString "IDENTITYCA_DB_CONNSTRING"

runWithConfig :: (Embed IO `Member` r, Error SomeException `Member` r) => Sem (Reader Config ': r) a -> Sem r a
runWithConfig f = do
  cfg <- loadConfig
  runReader cfg f
