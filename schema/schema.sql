-- This table has the information that is retained permanently about a person
-- to help ensure that two certificates aren't issued for the same person.
CREATE TABLE person (
  -- The public identifier for the person. This matches what goes in the
  -- certificate (essentially the Common Name).
  id UUID PRIMARY KEY NOT NULL,
  -- Current legal name, formatted according to the rules of any country, as
  -- found on an evidence document. NULL if erased for privacy reasons
  -- (not strictly needed for matching unlike the NOT NULL fields, but likely
  -- needed for administrative reasons).
  current_legal_name TEXT,
  -- First ever full legal name of the person prior to any name change for
  -- any reason.
  -- Formatted according to the rules of the country of birth at the time
  -- of registration of birth.
  original_legal_name TEXT NOT NULL,
  -- Date of birth of the person.
  date_of_birth DATE NOT NULL,
  -- The country of birth - Alpha-2 ISO-3166
  country_of_birth VARCHAR(2) NOT NULL,
  -- The place of birth within the country.
  -- In the format used by the country of birth at the time of registration of
  -- birth. May sometimes just be the country name.
  place_of_birth TEXT NOT NULL,
  -- Current contact email for the person. NULL if erased for privacy reasons.
  contact_email TEXT,
  -- When did we create this record
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  -- Not all person records are verified yet. If this one is, we record when from.
  verified_at TIMESTAMP WITH TIME ZONE,
  -- If invalid from a certain time, we record when from.
  -- This doesn't automatically imply revocation of certificates from that
  -- time (but in some cases both will happen), only that no new certificates
  -- will be issued.
  revoked_at TIMESTAMP WITH TIME ZONE
);

-- A request for a certificate to be generated  
CREATE TABLE certificate_request (
  id UUID PRIMARY KEY NOT NULL,
  -- Evidence of identity that is used to verify the request and then deleted.
  -- It is an encrypted gzipped tarball.
  private_evidence BYTEA,
  -- The claimed person identifier (can be unverified for a new person request,
  -- or refer to an existing one - but claim might be false).
  claimed_person UUID NOT NULL REFERENCES person(id),
  -- When did we create this record
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  -- When did we process the record
  processed_at TIMESTAMP WITH TIME ZONE,
  -- What was the result of processing
  --  'APPROVED' or 'DECLINED' (NULL if not processed)
  process_result TEXT,
  -- The public key - NULL if this is a revoke only request.
  public_key BYTEA
);
CREATE INDEX idx_cert_req_by_result ON certificate_request (process_result);

-- Gap-free sequences
CREATE TABLE gapfree_sequences (
  id TEXT PRIMARY KEY,
  next_value BIGINT NOT NULL
);

-- An issued certificate
CREATE TABLE certificate (
  -- An identifier, which is a gap-free sequence (not a normal sequence) because
  -- it is used to pack certificates.
  id BIGINT PRIMARY KEY,
  -- Request
  create_request UUID NOT NULL REFERENCES certificate_request (id),
  certificate BYTEA NOT NULL,
  -- When it is was valid from - never backdated.
  valid_from TIMESTAMP WITH TIME ZONE NOT NULL,
  -- When it is was revoked - never backdated.
  valid_to TIMESTAMP WITH TIME ZONE,
  -- The version where it was changed
  changed_version BIGINT NOT NULL
);
CREATE INDEX idx_certificate_changed_version_id ON certificate (changed_version, id);
